#!/bin/zsh
set -ex

# Tooling
./dotfiles/dotfiles.sh
./tools/essentials.sh
./tools/zsh.sh
./tools/gcp/gcloud.sh
./tools/node.sh

# Python
./python/pyenv.sh
./python/virtualenvwrapper.sh

# Golang
zsh golang/gvm.sh
zsh golang/go.sh