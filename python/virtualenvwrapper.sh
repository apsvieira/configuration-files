#!/bin/bash
set -ex
PYTHON_VERSION="3.11.0"
pyenv install $PYTHON_VERSION
pyenv global $PYTHON_VERSION

pip install --upgrade pip virtualenvwrapper

echo '' >> ~/.zshrc
echo '# virtualenvwrapper configuration #' >> ~/.zshrc
echo 'export WORKON_HOME="$HOME/environments/python"' >> ~/.zshrc
echo "source $HOME/.pyenv/versions/$PYTHON_VERSION/bin/virtualenvwrapper.sh" >> ~/.zshrc