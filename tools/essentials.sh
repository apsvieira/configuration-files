#!/bin/bash
set -ex

sudo apt-get update && sudo apt-get upgrade
sudo apt-get install -y \
    git curl vim htop
