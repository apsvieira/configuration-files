#!/bin/bash

curl -L https://raw.githubusercontent.com/warrensbox/terraform-switcher/release/install.sh | sudo bash


echo '' >> $HOME/.zshrc
echo '# Terraform #' >> $HOME/.zshrc
echo 'alias tfswitch="tfswitch -b $HOME/.terraform.versions/terraform"' >> $HOME/.zshrc
echo 'alias tf="$HOME/.terraform.versions/terraform"' >> $HOME/.zshrc
