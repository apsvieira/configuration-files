#!/bin/zsh
set -ex

sudo apt-get install -y \
    curl git mercurial make binutils bison gcc build-essential
    
zsh < <(curl -s -S -L https://raw.githubusercontent.com/moovweb/gvm/master/binscripts/gvm-installer)

echo "" >> $HOME/.zshrc
echo "# Golang #" >> $HOME/.zshrc
echo "## GVM ##" >> $HOME/.zshrc
echo '[[ -s "/home/apsvieira/.gvm/scripts/gvm" ]] && source "/home/apsvieira/.gvm/scripts/gvm"' >> $HOME/.zshrc

source $HOME/.gvm/scripts/gvm