#!/bin/zsh

set -ex

echo "" >> ~/.zshrc
echo "## Golang Environment ##" >> ~/.zshrc
echo 'export GOPATH="$HOME/go"' >> ~/.zshrc

gvm install go1.4 -B
gvm use go1.4
export GOROOT_BOOTSTRAP=$GOROOT
gvm install go1.19